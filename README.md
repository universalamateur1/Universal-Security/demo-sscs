# Flask App explictly Vulnerbale for SBOM

For the Demo Project for Software Supply Chain Security, Software Composition Analysis (SCA) via Dependency Scanning and Container Scanning In GitLAb

## Commands for local installation and prep

* Git Clone this repo
* virtual enviroment `python3 -m venv .`
* Install reqs `python3 -m  pip install -r requirements.txt`
* run app `python3 -m flask run`

## Artifial vulns

### Werkzeug

* [werkzeug@0.15.1 vulnerabilities](https://security.snyk.io/package/pip/werkzeug/0.15.1) - version 0.15.1

### Flask

* [flask@0.12.2 vulnerabilities](https://security.snyk.io/package/pip/flask/0.12.2) - 0.12.2

### Image

* [python:3.8-slim-buster](https://hub.docker.com/layers/library/python/3.8-slim-buster/images/sha256-4bc25e1810c14af78ca0235ff6f63740113540fd49f8dc5bc6c7b456572b5806?context=explore)
